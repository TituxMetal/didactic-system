<?php

namespace App\Controllers;

use Core\Controller\Controller;

class HomeController extends Controller {

	public function index() {
		$test = $this->db->table('test')->get();

		return $this->view->render($this->response, 'home.tw', compact('test'));
	}

}
