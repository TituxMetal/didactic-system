<?php

namespace Modules\Database;

use Illuminate\Database\Capsule\Manager;

class DatabaseManager extends Manager {

	private $dbOptions;

	public function __construct($options) {
		$this->dbOptions = $options;
		parent::__construct();
		$this->addConnection($this->dbOptions);
		$this->setAsGlobal();
		$this->bootEloquent();

		return $this;
	}

}