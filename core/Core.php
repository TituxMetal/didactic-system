<?php

namespace Core;

use Core\Config\Config;
use Core\Config\Parser\JsonParser;
use Core\Config\Parser\PhpParser;
use Slim\App as Slim;

/**
  * Core that extends the Slim Micro Framework
  *
  */
class Core extends Slim {

	protected $config = [];
	protected $viewPath;

	/**
	  * __construct()
	  * Adding the settings and Twig view support for Slim
		* @return void
	  */
	public function __construct() {
		session_start();
		$this->container = $this->getContainer();
		$this->config = new Config(new JsonParser);
		$this->config->load(CONFIG_PATH);
		parent::__construct($this->config->get('slimOptions'));
		$this->addDependencies(ROOT . '/app/services.php');

		return;
	}

	public function getViewPath() {
		if(!is_array($this->config->get('viewsOptions.path'))) {
			return $this->viewPath = ROOT . $this->config->get('viewsOptions.path');
		}

		foreach($this->config->get('viewsOptions.path') as $k => $v) {
			$this->viewPath[] = ROOT . $v;
		}

		return $this->viewPath;
	}

	protected function addDependencies($file) {
		require ROOT . '/core/services.php';
		if(file_exists($file)) {
			require $file;
		}

		return $this;
	}

}