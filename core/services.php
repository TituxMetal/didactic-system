<?php

use Core\Helpers\Hash;
use Core\Errors\NotFoundHandler;
use Core\Middlewares\CsrfViewMiddleware;
use Core\Middlewares\InputDataMiddleware;

use Modules\Database\DatabaseManager;

use RandomLib\Factory as RandomLib;
use Slim\Csrf\Guard;
use Slim\Flash\Messages as Flash;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

$container = $this->getContainer();

// Add the flash messages dependency to the container
$container['flash'] = function($container) {

	return new Flash();
};

// Add the Twig dependency to the container
$container['view'] = function($container) {
	$viewPath = $this->getViewPath();
	$this->view = new Twig($viewPath, [
		$this->config->get('viewsOptions.cache'),
	]);

	$this->view->addExtension(new TwigExtension(
		$container->router,
		$container->request->getUri()
	));

	$this->view->getEnvironment()->addGlobal('flash', $container->flash);

	return $this->view;
};

$container['notFoundHandler'] = function($container) {

	return new NotFoundHandler(
		$container->get('view'),
		'errors/404.tw',
		function($request, $response) use($container) {
			return $container['response']->withStatus(404);
		}
	);
};

// Add the database connection to the container
$container['db'] = function($container) {

	return new DatabaseManager($this->config->get('dbOptions'));
};

// Add the random lib generator to the container
$container['randomlib'] = function($container) {
	$factory = new RandomLib;

	return $factory->getMediumStrengthGenerator();
};

$container['hash'] = function($container) {

	return new Hash($this->config);
};

$container['csrf'] = function($container) {

	return new Guard;
};

$this->add(new CsrfViewMiddleware($container));
$this->add(new InputDataMiddleware($container));

$this->add($container->csrf);