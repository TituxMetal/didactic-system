<?php

namespace Core\Middlewares;

class InputDataMiddleware extends Middleware {

	public function __invoke($request, $response, $next) {
		$_SESSION['inputData'] = $request->getParams();
		$this->container->view->getEnvironment()->addGlobal('inputData', $_SESSION['inputData']);
		unset($_SESSION['inputData']);

		$response = $next($request, $response);

		return $response;
	}

}