<?php

namespace Core\Config;

use Core\Config\Contract\ParserInterface;

class Config {

	private $parser;
	protected $config;

	public function __construct(ParserInterface $parser) {
		$this->setParser($parser);

		return $this;
	}

	public function setParser(ParserInterface $parser) {
		$this->parser = $parser;
	}

	public function load($file) {
		$this->config = $this->parser->parse($file);

		return $this;
	}

	public function get($key, $default = null) {
		$keys = explode('.', $key);
		$config = $this->config;

		foreach($keys as $key) {
			if(isset($key, $config[$key])) {
				$config = $config[$key];
				continue;
			} else {
				$config = $default;
				break;
			}
		}

		return $config;
	}

}