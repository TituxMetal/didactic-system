<?php

namespace Core\Config\Contract;

interface ParserInterface {

	public function parse($file);

}