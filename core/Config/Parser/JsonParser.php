<?php

namespace Core\Config\Parser;

use Core\Config\Contract\ParserInterface;

class JsonParser implements ParserInterface {

	public function parse($file) {

		return json_decode(file_get_contents($file), true);
	}

}