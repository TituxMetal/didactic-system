<?php

namespace Core\Config\Parser;

use Core\Config\Contract\ParserInterface;

class PhpParser implements ParserInterface {

	public function parse($file) {

		return require $file;
	}

}