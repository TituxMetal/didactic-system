<?php

return [
	'app' => [
		'hash' => [
			'cost' => 12,
		],
	],
	'dbOptions' => [
		'driver' => 'mysql',
		'host' => 'localhost',
		'database' => 'database_name',
		'username' => 'your_username',
		'password' => 'your_password',
		'charset' => 'utf8',
		'collation' => 'utf8_unicode_ci',
		'prefix' => '',
		'port' => 3306,
	],
	'slimOptions' => [
		'settings' => [
			'displayErrorDetails' => true,
		],
	],
	'viewsOptions' => [
		'engine' => 'twig',
		'path' => [
			'/app/resources/views/',
			'/resources/views/'
		],
		'cache' => false,
	],
];